<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function(){

	Route::get('/user', function (Request $request) {
   		 return $request->user();
	});

});


Route::match(['GET'], '/products/{page?}/{maxcount?}', [
    'uses' => 'ProductsController@listProducts'
])->where('page', '[0-9]+');


Route::match(['GET'], '/products/{id}', [
    'uses' => 'ProductsController@getProductDetails'
])->where('id', '[a-z0-9-]{1,}');

/*
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\AuthController@login');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::post('products', 'ProductsController@addProduct');
    });
});
*/
Route::match(['POST'], '/signin', [
    'uses' => 'Auth\AuthController@login'
]);


Route::match(['POST'], '/products', [
	'middleware' => ['auth:api'],
    'uses' => 'ProductsController@addProduct'
]);
