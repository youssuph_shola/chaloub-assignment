<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Products;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Products::truncate();
		
        $faker = Factory::create();
				
        for ($i = 0; $i < 100; $i++) {
            Products::create([
				'sku' => strtoupper($faker->bothify('??#?###??#?##??')), 
				'title' => $faker->sentence, 
				'url' => $faker->url, 
				'abstract' => $faker->sentence,
			    'description' => $faker->paragraph, 
				'price' => $faker->randomNumber(3), 
				'image_url' => $faker->url,
				'stock' => $faker->randomNumber(2)
            ]);
        }		
		
    }
}
