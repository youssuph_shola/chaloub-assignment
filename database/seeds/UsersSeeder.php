<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
		
        $faker = Factory::create();
				
        for ($i = 0; $i < 10; $i++) {
            User::create([
				'name' => $faker->name, 
				'email' => $faker->email, 
				'password' => Hash::make('user_testing'),
			    'api_token' => str_random(65)
				
            ]);
        }		
		
    }
}
