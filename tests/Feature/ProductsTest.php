<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Products;
use DB;


class ProductsTest extends TestCase
{


	use DatabaseTransactions;
	use WithFaker;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testProductListingForSuccess()
    {
    	$headers = ['Accept' => 'application/json',
            'X-Requested-With'=>'XMLHttpRequest'];


       $this->json('GET', 'api/products', [], $headers)
        ->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testProductListingForEmptyResult()
    {
    	$headers = ['Accept' => 'application/json',
            'X-Requested-With'=>'XMLHttpRequest'];
       $total = Products::count();
       $perPage = 10;
       $page  = $total/$perPage;
       $perPage = $perPage +50;
       $this->json('GET', "api/products/$page/$perPage", [], $headers)
        ->assertStatus(404);
    }

    public function testCreateProductFailedValidation()
    {

    		//$user = factory(\App\User::class)->create();
	        $user = DB::table('users')->first();

	        $payload = ['email'=>$user->email,
	    				'password'=>'user_testing',
	    				'remember_me'=>true];


	    	$headers = ['Content-Type'=>'application/json',
	    				'Accept' => 'application/json',
			            'X-Requested-With'=>'XMLHttpRequest'];
            
            $response = $this->json('POST', 
			            			'api/signin', 
			            			$payload, 
			            			$headers);

            $authToken = $response->getData();
           
            $token = $authToken->access_token;

			$headers['Authorization'] = "Bearer $token";

			$data = factory(\App\Products::class)->create();
			
            $response = $this->json('POST', '/api/products', $data->toArray(), $headers);
            $response->assertStatus(422);

      }


    public function testCreateProductSuccess()
    {

	        $user = DB::table('users')->first();

	        $payload = ['email'=>$user->email,
	    				'password'=>'user_testing',
	    				'remember_me'=>true];


	    	$headers = ['Content-Type'=>'application/json',
	    				'Accept' => 'application/json',
			            'X-Requested-With'=>'XMLHttpRequest'];
            
            $response = $this->json('POST', 
			            			'api/signin', 
			            			$payload, 
			            			$headers);

            $authToken = $response->getData();
           
            $token = $authToken->access_token;

			$headers['Authorization'] = "Bearer $token";

			$data = ['sku' => strtoupper($this->faker->bothify('??#?###??#?##??')), 
					'title' => $this->faker->sentence, 
					'url' => $this->faker->url, 
					'abstract' => $this->faker->sentence,
				    'description' => $this->faker->paragraph, 
					'price' => $this->faker->randomNumber(3), 
					'image_url' => $this->faker->url,
					'stock' => $this->faker->randomNumber(2)];

			
            $response = $this->json('POST', '/api/products', $data, $headers);
           // fwrite(STDERR, print_r($response->getData(), TRUE));
            $response->assertStatus(200);
            $response->assertJson(['status' => 'success']);
      }



    public function testCreateProductFailure()
    {


	    	$headers = ['Accept' => 'application/json',
			            'X-Requested-With'=>'XMLHttpRequest'];

			$data = factory(\App\Products::class)->create();

            $response = $this->json('POST', '/api/products', $data->toArray(), $headers);
            $response->assertStatus(401)
            ->assertJson(["message"=>"Unauthenticated."]);
      }


    public function testGetProductDetailsForSuccess()
    {
    	$headers = ['Accept' => 'application/json',
            'X-Requested-With'=>'XMLHttpRequest'];

       	$product = Products::take(1)->first();

       $this->json('GET', 'api/products', [$product->id], $headers)
        ->assertStatus(200)
        ->assertJsonStructure([
            ["id",
	        "sku",
	        "title",
	        "url",
	        "abstract",
	        "description",
	        "price",
	        "image_url",
	        "stock",
	        "created_at",
	        "updated_at"]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetProductDetailsForEmptyResult()
    {
    	$headers = ['Accept' => 'application/json',
            'X-Requested-With'=>'XMLHttpRequest'];

       $this->json('GET', 'api/products/null', [], $headers)
        ->assertStatus(404)
        ->assertJson(["data"=> "Resource not found"]);
    }      

}
