<?php

namespace App\Listeners\Events\Products;

use App\Events\Products\AddNewProduct;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Products;

class AddProduct
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddNewProduct  $event
     * @return void
     */
    public function handle(AddNewProduct $event)
    {
       $data = $event->getProductDetails();
       Products::addProduct($data);
    }
}
