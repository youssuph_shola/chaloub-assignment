<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use Response;
use \Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\AddProductRequest;
use App\Events\Products\AddNewProduct;

class ProductsController extends Controller
{

    public function __construct()
    {

    }

    public function listProducts($page = null, $productCount = null){

       $result = Products::listProducts($page, $productCount);

       if(!$result)
            throw new ModelNotFoundException;

       return Response::json($result);
    }


    public function getProductDetails($productId){

       $result = Products::getProductDetails($productId);

       if(!$result)
            throw new ModelNotFoundException;

       return Response::json($result[0]);
    }


    public function addProduct(AddProductRequest $request){
        if($request->validated()){
          $outcome = event(new AddNewProduct($request->all()));  
          return Response::json(['status'=>'success']);     
        }

    }



}
