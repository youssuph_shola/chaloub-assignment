<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            return [
                'sku' => 'bail|required|alpha_num|min:15|max:15|unique:products',
                'title' => 'required|min:3',
                'url' => 'required|url',
                'abstract' => 'required|min:6',
                'description' => 'required|min:10',
                'price' => 'required|numeric',
                'image_url' => 'required|url',
                'stock' => 'required|integer'
            ];
        }
    }
}
