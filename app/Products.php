<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Products extends Model
{

	use Uuids;
	
    public $incrementing = false;
	
	protected $fillable = ['sku', 'title', 'url', 'abstract',
						   'description', 'price', 'image_url',
						   'stock'];



	public static function listProducts($page, $productPerPage){

		$page = empty($page) ? 0 : (int)$page-1;
		$productPerPage = empty($productPerPage) ? 15 : (int)$productPerPage;
		$offset = $productPerPage * $page;

        $result = DB::table('products')->skip($offset)->take($productPerPage)->get();
        return (count($result) > 0)? $result : null;
	}



	public static function getProductDetails($productId){

        $result = DB::table('products')->where('id', $productId)->get();
        return (count($result)>0) ? $result : null;
	}

	public static function addProduct($productDetails){
		Products::create($productDetails);
	}
}
