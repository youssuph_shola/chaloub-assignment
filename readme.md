**INSTALLATION**

1). Download Project

2). cd into laravel folder

3). Run `docker-compose up -d`

4). Run `composer install`

5). Run `docker-compose exec app composer initApp`


**API DOCUMENTATION**

The following APIs are available

>'GET' `/api/products`

This endpoint doesn't need any parameter to function however as specified in the assignment, two optional parameters can be passed to it to fine tune the result set produced.

The first Parameter is the page number, and the second parameter is the number of results to be displayed in that page.
An example is - `/api/products/2/10`. 
In this example, 2 is the result offset and 10 is the number of results that will be displayed.

>'GET' `/api/products/{id}`
This endpoint requires and id (uuid) to load the content of a single product from the database.


>'POST' `/api/products/`
This endpoint requires a user to have been authenticated first before it can be reached. Hence passport oAuth was implemented.

**Authenitication**

So a valid email address and password is needed first to get an access token.
To obtain the access token, a json data in the format below should be sent as a post request to the following endpoint (`/api/signin`)

>{"email":some-email,
 "password": some-password,
 "remember_me": true}
 
 Also remember to set the headers for `contentType` and `Accept` both to `application/json`
 
 If the username and password combination is correct, the (`/api/signin`) endpoint responds with a `json` data that contains an `access token`  in the format below
 
> { ...., 
 "access_token": some-access-token, 
 .... }
 
**Post Product Data to Endpoint**

 This access token should be retrieved and attached as an `Authorization` header by concatenating the word `Bearer` to the Token so that it looks something like this `Bearer $access_token`
 
 Once access token is attached to the header, data can be posted to the endpoint and the endpoint should return `success`. The only exception to this is when `validation` test fails and server returns 
 status code `422`.
 
 
 
**TESTING**
 
To run the Functional test, `cd` into the `laravel` directory and simply run `composer test`.




